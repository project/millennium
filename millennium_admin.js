
Drupal.behaviors.millennium_admin_queue = (function () {
  $("#range-fieldset").hide();
  $("input[@name=\'source\']").change(
    function() {
      //alert("changed!");
      var choice = $("input[@name=\'source\']:checked").val();
      if (choice == "list") {
        $("#range-fieldset").hide();
        $("#list-fieldset").show();
        $("#edit-offline-refresh-wrapper").hide();
        $("input[@name=\'offline_refresh\']").change();
        $("#baseurl-table").show();
        $("#query-fieldset").hide();
        $("#url-fieldset").hide();
        $("#import-options").show();
        $("#edit-using-cron").show();
      }
      if (choice == "range") {
        $("#range-fieldset").show();
        $("#list-fieldset").hide();
        $("#edit-offline-refresh-wrapper").hide();
        $("#options-fieldset").show();
        $("#edit-using-cron").show();
        $("#baseurl-table").show();
        $("#query-fieldset").hide();
        $("#url-fieldset").hide();
        $("#import-options").show();
        $("#edit-using-cron").show();
      }
      if (choice == "query") {
        $("#range-fieldset").hide();
        $("#list-fieldset").hide();
        $("#edit-offline-refresh-wrapper").hide();
        $("#baseurl-table").show();
        $("#query-fieldset").show();
        $("#url-fieldset").hide();
        $("#import-options").show();
        $("#edit-using-cron").hide();
      }
       if (choice == "url") {
        $("#range-fieldset").hide();
        $("#list-fieldset").hide();
        $("#edit-offline-refresh-wrapper").hide();
        $("#baseurl-table").hide();
        $("#query-fieldset").hide();
        $("#url-fieldset").show();
        $("#import-options").show();
        $("#edit-using-cron").hide();
      }
      if (choice == "existing") {
        $("#range-fieldset").hide();
        $("#list-fieldset").hide();
        $("#edit-offline-refresh-wrapper").show();
        $("#baseurl-table").hide();
        $("#query-fieldset").hide();
        $("#url-fieldset").hide();
        $("#import-options").show();
        $("#edit-using-cron").show();
      }
      if (choice == "test") {
        $("#range-fieldset").hide();
        $("#list-fieldset").hide();
        $("#edit-offline-refresh-wrapper").hide();
        $("#baseurl-table").hide();
        $("#query-fieldset").hide();
        $("#url-fieldset").hide();
        $("#import-options").hide();
        $("#edit-using-cron").hide();
      }
      if (choice == "queued") {
        $("#range-fieldset").hide();
        $("#list-fieldset").hide();
        $("#edit-offline-refresh-wrapper").show();
        $("#baseurl-table").hide();
        $("#query-fieldset").hide();
        $("#url-fieldset").hide();
        $("#import-options").show();
        $("#edit-using-cron").hide();
      }
      $(this).blur();
    }
  );
  // Activate on page load.
  $("input[@name=\'source\']").change();
});